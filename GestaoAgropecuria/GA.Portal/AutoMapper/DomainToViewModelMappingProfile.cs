﻿using AutoMapper;
using GA.Dominio.Entidades;
using GA.Portal.Areas.Basico.ViewModels.Animal;


namespace GA.Portal.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            
            #region Cadastro Basico
                CreateMap<Animal, AnimalIndexViewModel>();
          
            #endregion

        }
    }
}