﻿using AutoMapper;
using GA.Dominio.Entidades;
using GA.Portal.Areas.Basico.ViewModels.Animal;

namespace GA.Portal.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            #region Cadastros Basicos
            CreateMap<AnimalIndexViewModel, Animal>();
          
            #endregion
        }
    }
}