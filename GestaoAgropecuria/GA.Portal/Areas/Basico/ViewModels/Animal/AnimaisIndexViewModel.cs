﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GA.Portal.Areas.Basico.ViewModels.Animal
{
    public class AnimalIndexViewModel
    {
        [Key]
        public int AnimalId { get; set; }

        [Required(ErrorMessage = "")]
        [MaxLength(100)]
        [DisplayName("SISBOV")]
        public Int64 Sisbov { get; set; }

        [Required(ErrorMessage = "")]
        [MaxLength(200)]
        [DisplayName("RAÇA")]
        public string Raca { get; set; }

        [Required(ErrorMessage = "")]
        [MaxLength(200)]
        [DisplayName("DATA ENTRADA")]
        public DateTime DtEntrada { get; set; }

        [Required(ErrorMessage = "")]
        [MaxLength(200)]
        [DisplayName("PESO ENTRADA")]
        public int PesoEntrada { get; set; }

        [Required(ErrorMessage = "")]
        [MaxLength(200)]
        [DisplayName("DATA ULT. PESO")]
        public DateTime DtUltimoPeso { get; set; }

        [Required(ErrorMessage = "")]
        [MaxLength(200)]
        [DisplayName("ULTIMO PESO")]
        public int UltimoPeso { get; set; }

        [Required(ErrorMessage = "")]
        [MaxLength(200)]
        [DisplayName("COMUNICADO")]
        public bool Comunicado { get; set; }

        [Required(ErrorMessage = "")]
        [MaxLength(200)]
        [DisplayName("STATUS")]
        public bool Status { get; set; }

        [Required(ErrorMessage = "")]
        [DisplayName("VALOR POR CABEÇA")]
        public decimal ValorCabeca { get; set; }


    }
}