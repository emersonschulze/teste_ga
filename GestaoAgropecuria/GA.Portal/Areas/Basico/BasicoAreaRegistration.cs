﻿using System.Web.Mvc;

namespace GA.Portal.Areas.Basico
{
    public class BasicoAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Basico";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.IgnoreRoute("{resource}.axd/{*pathinfo}");


           context.MapRoute(
                "Animal_default",
                "Basico/Animal/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}