﻿using AutoMapper;
using GA.Dominio.Interfaces.Servicos;
using GA.Portal.Areas.Basico.ViewModels.Animal;
using GA.Portal.AutoMapper;
using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace GA.Portal.Areas.Basico.Controllers.Animal
{

    public class AnimaisController : Controller
    {
        private readonly IAnimalService _appService;
        private readonly IMapper _mapper;

        public AnimaisController(IAnimalService repositorio)
        {
            this._appService = repositorio;
            _mapper = AutoMapperConfig.Mapper;

        }


        //GET: Animal
        public ActionResult Index()
        {
            var animalViewModel = _mapper.Map<IEnumerable<Dominio.Entidades.Animal>, IEnumerable<AnimalIndexViewModel>>(_appService.BuscaTodos());
            return View(animalViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upload(System.Web.HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {

                if (upload != null && upload.ContentLength > 0)
                {

                    if (upload.FileName.EndsWith(".csv"))
                    {
                        Stream stream = upload.InputStream;
                        
                        using (StreamReader sr = new StreamReader(stream))
                        {
                          Importar(sr);
                        }                     
                    }
                    else
                    {
                        ModelState.AddModelError("Arquivo", "Não é suportado");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("Arquivo", "Selecione um arquivo");
                }
            }
            return View();
        }

        public ActionResult ExportCsv()
        {
            string csv = _appService.BuscaPrimeiros(50).ToList().ToString();
           return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "export.csv");
        }

        private ActionResult Importar(StreamReader csvTable)
        {
                for (int i = 0; i < csvTable.ReadLine().Count(); i++)
                {
               
                    string[] item = csvTable.ReadLine().Split(';');
                    if (!item.Contains("sisbov"))
                    {
                        var animal = new Dominio.Entidades.Animal();
                        animal.Sisbov = Int64.Parse(item.GetValue(0).ToString());
                        animal.Raca = item.GetValue(1).ToString();
                        animal.DataEntrada = DateTime.Parse(item.GetValue(2).ToString());
                        animal.PesoEntrada = int.Parse(item.GetValue(3).ToString());
                        animal.DataUltimoPeso = DateTime.Parse(item.GetValue(4).ToString());
                        animal.UltimoPeso = int.Parse(item.GetValue(5).ToString());
                        animal.Comunicado = bool.Parse(item.GetValue(6).ToString());
                        animal.Status = bool.Parse(item.GetValue(7).ToString() == "ativo" ? bool.TrueString : bool.FalseString);
                        animal.ValorArroba = decimal.Parse(item.GetValue(8).ToString());

                        var existe = _appService.BuscarSisbov(animal.Sisbov);

                        if (existe.ToList().Count == 0)
                        {
                            _appService.Incluir(animal);
                        }
                    }
               
            }

           
            var animalViewModel = _mapper.Map<IEnumerable<Dominio.Entidades.Animal>, IEnumerable<AnimalIndexViewModel>>(_appService.BuscaTodos());
                return View(animalViewModel);
         }

    }
}
