﻿using GA.Dominio.Entidades;
using GA.Dominio.Interfaces.Repositorios;
using GA.Dominio.Interfaces.Servicos;
using System;
using System.Collections.Generic;
using System.ServiceProcess;

namespace GA.Dominio.Servicos
{
    public class AnimalService : ServiceBase<Animal>, IAnimalService
    {
        private readonly IAnimalRepository _repository;

        public AnimalService(IAnimalRepository animalRepository) : base(animalRepository)
        {
            _repository = animalRepository;
        }

        public IEnumerable<Animal> BuscarSisbov(Int64 numero)
        {
            return _repository.BuscarSisbov(numero);
        }
    }
}
