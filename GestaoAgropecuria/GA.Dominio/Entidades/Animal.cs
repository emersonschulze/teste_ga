﻿
using System;

namespace GA.Dominio.Entidades
{
    public class Animal
    {
        public int AnimalId { get; set; }

        public Int64 Sisbov { get; set; }

        public string Raca { get; set; }

        public DateTime DataEntrada { get; set; }

        public int PesoEntrada { get; set; }
        public DateTime DataUltimoPeso { get; set; }
        public int UltimoPeso { get; set; }

        public bool Comunicado { get; set; }

        public bool Status { get; set; }

        public decimal ValorArroba{ get; set; }

    }
}
