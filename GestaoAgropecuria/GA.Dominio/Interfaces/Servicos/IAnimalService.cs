﻿using GA.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace GA.Dominio.Interfaces.Servicos
{
    public interface IAnimalService : IServiceBase<Animal>
    {
        IEnumerable<Animal> BuscarSisbov(Int64 numero);
    }
}
