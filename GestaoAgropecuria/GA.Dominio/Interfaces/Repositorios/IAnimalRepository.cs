﻿using GA.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace GA.Dominio.Interfaces.Repositorios
{
    public interface IAnimalRepository : IRepositoryBase<Animal>
    {
        IEnumerable<Animal> BuscarSisbov(Int64 numero);
    }
}
