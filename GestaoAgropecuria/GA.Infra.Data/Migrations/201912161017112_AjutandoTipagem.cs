﻿namespace GA.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AjutandoTipagem : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Animal", "Sisbov", c => c.Long(nullable: false));
            AlterColumn("dbo.Animal", "UltimoPeso", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Animal", "UltimoPeso", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.Animal", "Sisbov", c => c.Int(nullable: false));
        }
    }
}
