﻿namespace GA.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CriandoBase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animal",
                c => new
                    {
                        AnimalId = c.Int(nullable: false, identity: true),
                        Sisbov = c.Int(nullable: false),
                        Raca = c.String(maxLength: 255, unicode: false),
                        DataEntrada = c.DateTime(nullable: false),
                        PesoEntrada = c.Int(nullable: false),
                        DataUltimoPeso = c.DateTime(nullable: false),
                        UltimoPeso = c.String(maxLength: 255, unicode: false),
                        Comunicado = c.Boolean(nullable: false),
                        Status = c.Boolean(nullable: false),
                        ValorArroba = c.Decimal(nullable: false, precision: 10, scale: 2),
                    })
                .PrimaryKey(t => t.AnimalId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Animal");
        }
    }
}
