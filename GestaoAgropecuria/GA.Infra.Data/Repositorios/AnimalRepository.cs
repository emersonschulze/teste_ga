﻿using GA.Dominio.Entidades;
using GA.Dominio.Interfaces.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;


namespace GA.Infra.Data.Repositorios
{
    public class AnimalRepository : RepositoryBase<Animal>, IAnimalRepository
    {

        public IEnumerable<Animal> BuscarSisbov(Int64 numSisbov)
        {
            return Bd.Animais.Where(c => c.Sisbov == numSisbov);
        }

    }
}
