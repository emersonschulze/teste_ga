﻿using GA.Dominio.Entidades;
using System.Data.Entity.ModelConfiguration;

namespace GA.Infra.Data.EntityConfig
{
    public class AnimalConfiguration : EntityTypeConfiguration<Animal>
    {
        public AnimalConfiguration()
        {
            HasKey(c => c.AnimalId);

            Property(c => c.Sisbov).IsRequired();
            Property(c => c.Raca).HasMaxLength(255);
            Property(c => c.PesoEntrada);
            Property(c => c.UltimoPeso);
            Property(c => c.DataEntrada);
            Property(c => c.DataUltimoPeso);
            Property(c => c.Comunicado);
            Property(c => c.Status);
            Property(c => c.ValorArroba).HasPrecision(10,2);
        }
    }
}
