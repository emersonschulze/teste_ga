﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GA.Infra.Data.Utils
{
    public class CvsHelper
    {
        public IEnumerable<string> ImportCVS()
        {
            var lines = File.ReadAllLines(@"C:\Users\emers\OneDrive\Área de Trabalho\imp1.cvs").Select(x => x.Split(';'));
            int lineLength = lines.First().Count();
            var CSV = lines.Skip(1)
                       .SelectMany(x => x)
                       .Select((v, i) => new { Value = v, Index = i % lineLength })
                       .Where(x => x.Index == 2 || x.Index == 3)
                       .Select(x => x.Value);
            return CSV;
        }

    }
}
