﻿using GA.Dominio.Interfaces.Repositorios;
using GA.Dominio.Interfaces.Servicos;
using GA.Dominio.Servicos;
using GA.Infra.Data.Repositorios;
using SimpleInjector;


namespace Ga.Infra.IOC
{
    public class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            #region Services
            container.Register(typeof(IServiceBase<>), (typeof(ServiceBase<>)), Lifestyle.Scoped);
            container.Register<IAnimalService, AnimalService>();
            #endregion

            #region Repositorios
            container.Register(typeof(IRepositoryBase<>), (typeof(RepositoryBase<>)), Lifestyle.Scoped);
            container.Register<IAnimalRepository, AnimalRepository>();
            #endregion
        } 
    }
}